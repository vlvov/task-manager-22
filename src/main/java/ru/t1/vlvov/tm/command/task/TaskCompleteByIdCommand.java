package ru.t1.vlvov.tm.command.task;

import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Complete task by Id.";

    private final String NAME = "task-complete-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

}