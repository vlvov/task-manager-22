package ru.t1.vlvov.tm.command.user;

import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    private final String NAME = "user-lock";

    private final String DESCRIPTION = "Lock user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

}
