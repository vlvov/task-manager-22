package ru.t1.vlvov.tm.command.project;

import ru.t1.vlvov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Create project.";

    private final String NAME = "project-create";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getProjectService().create(userId, name, description);
    }

}