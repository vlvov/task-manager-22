package ru.t1.vlvov.tm.command.task;

import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Display all tasks.";

    private final String NAME = "task-list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("SELECT SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortName = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortName);
        final String userId = getAuthService().getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}