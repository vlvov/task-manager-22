package ru.t1.vlvov.tm.command.project;

import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Show project by Index.";

    private final String NAME = "project-show-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getAuthService().getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}