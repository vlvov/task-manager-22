package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> , IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByProjectId(String userId, String projectId);

}
