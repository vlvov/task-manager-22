package ru.t1.vlvov.tm.api.service;

import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User lockUserByLogin(String id);

    User unlockUserByLogin(String id);

}
