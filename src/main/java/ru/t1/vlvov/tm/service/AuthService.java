package ru.t1.vlvov.tm.service;

import ru.t1.vlvov.tm.api.service.IAuthService;
import ru.t1.vlvov.tm.api.service.IUserService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.field.LoginEmptyException;
import ru.t1.vlvov.tm.exception.field.PasswordEmptyException;
import ru.t1.vlvov.tm.exception.user.AccessDeniedException;
import ru.t1.vlvov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.vlvov.tm.exception.user.PermissionException;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked() == true) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null) return;
        if (!Arrays.stream(roles).anyMatch(r -> r==getUser().getRole())) throw new PermissionException();
    }

}
